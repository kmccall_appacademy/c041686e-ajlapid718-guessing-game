# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  correct_number = rand(1..100)
  print "Guess a number: "
  guesses = 1
  until (guess = gets.chomp.to_i) == correct_number
    guesses += 1
    if guess < correct_number
      puts "#{guess} is too low!"
      print "Guess again: "
    else
      puts "#{guess} is too high!"
      print "Guess again: "
    end
  end
  print "#{guess} is the correct number. It took #{guesses} guesses."
end

# def file_shuffler
#   print "Type in a file name: "
#   input_name = gets.chomp
#   File.open("#{input_name}-shuffled.txt", "w") do |f|
#     File.readlines(input_name).shuffle.each do |line|
#       f.puts line.chomp
#     end
#   end
# end
